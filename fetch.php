<?php
function remoteExists($url) {
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  $o = curl_exec($ch);
  $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  return $httpCode === 200;
}

function saveId($id) {
  $startconf = fopen('startid.txt', 'r');
  $oid = (int)fread($startconf, filesize('startid.txt'));
  fclose($startconf);

  if ($oid < $id) {
    $startconf = fopen('startid.txt', 'w');
    fwrite($startconf, $id + 1);
    fclose($startconf);
  }
}

function fetchDoc($id, $remote, $key) {
  $url = 'http://www.cartoonnetwork.com/cnservice/cartoonsvc/content/xml/getContentById.do?contentId=' . $id;
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  $output = curl_exec($ch);
  curl_close($ch);

  try {
    $x = new SimpleXMLElement($output);

    // Ignore <Content> entries
    if ($x->getName() == 'Content') return;
  } catch (Exception $e) {
    $log = 'Error when parsing ' . $url . ': ' . $e->getMessage() . "\n";
    echo $log;

    $logfile = fopen('errors.log', 'a');
    fwrite($logfile, "\n" . $log);
    fclose($logfile);

    return;
  }

  if (!$x->showTitle || empty($x->showTitle)) return;

  $logfile = fopen('found.log', 'a');
  fwrite($logfile, "\n" . $id);
  fclose($logfile);

  if (!$x->lastAirDate || empty($x->lastAirDate)) {
    $airdate = null;
  } else {
    $dateobj = DateTime::createFromFormat('Y-m-d H:i:s', (string)$x->lastAirDate);
    $airdate = $dateobj->getTimestamp();
  }

  $images = [];
  foreach($x->Video_Thumb_1280_720 as $thumb) {
    $i = 'http://i.cdn.turner.com/v5cache/CARTOON/site/' . $thumb->srcUrl;

    if (!in_array($i, $images)) $images[] = $i;
  }

  $videos = [];
  $url = 'https://ht.cdn.turner.com/toon/big/preview/tve/' . $x->AssetId . '_B13R1.mp4';
  if (remoteExists($url)) $videos[] = $url;

  $url = 'https://ht.cdn.turner.com/toon/big/preview/tve/' . $x->AssetId . '_3500kbps_1280x720.mp4';
  if (remoteExists($url)) $videos[] = $url;

  $data = array(
    'id' => $id,
    'show' => (string)$x->showTitle,
    'title' => (string)$x->episodeTitle,
    'description' => (string)$x->VideoSynopsis,
    'airdate' => $airdate,
    'finddate' => time(),
    'images' => str_replace('http:', 'https:', implode(',', $images)),
    'videos' => implode(',', $videos),
    'xml' => $output
  );

  $data_string = json_encode($data);

  $ch = curl_init($remote);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
  curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'X-Api-Key: ' . $key,
    'Content-Type: application/json',
    'Content-Length: ' . strlen($data_string))
  );

  curl_exec($ch);

  saveId($id);
}

<?php
// run with progress: php lookup.php progress
$pro = ($argc > 1 && $argv[1] == 'progress');

require('checks.php');
require('fetch.php');
require('PHPTerminalProgressBar/PHPTerminalProgressBar.php');

$config = parse_ini_file('config.ini');

$startconf = fopen('startid.txt', 'r');
$startid = (int)fread($startconf, filesize('startid.txt'));
fclose($startconf);

$laststart = fopen('laststart.txt', 'w');
fwrite($laststart, date("Y-m-d H:i:s") . ' UTC');
fclose($laststart);

$end = $startid + $config['howmany'];

if ($pro) {
  echo 'Start: ' . $startid . ' @ ' . date("Y-m-d H:i:s") . ' UTC' . PHP_EOL;
  $pg = new PHPTerminalProgressBar($config['howmany'], '┤:bar├ - :current/:total (:percent%) ');
  $pg->symbolComplete = '█';
}

for ($i = $startid; $i < $end; $i++) {
  fetchDoc($i, $config['remote'], $config['key']);
  if ($pro) $pg->tick();
}

if ($pro) echo PHP_EOL . 'End: ' . $end . ' @ ' . date("Y-m-d H:i:s") . ' UTC' . PHP_EOL;

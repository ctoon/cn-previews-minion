# CN Previews Minion

> PHP Script that will search for previews early and submit them to the main backend.

## Installation

```bash
# clone repo recursively
git clone --recursive https://gitlab.com/ctoon/cn-previews-minion.git

# copy and edit config
cp config.ini.example config.ini
$EDITOR config.ini

# create a file with the id as a starting point
echo '1082555' > startid.txt

# Launch the script
php lookup.php
```

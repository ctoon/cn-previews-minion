<?php
// Get comments
$f = 'comments.txt';
$fh = fopen($f, 'r');
$comments = fread($fh, filesize($f));
fclose($fh);

// Get startid
$f = 'startid.txt';
$fh = fopen($f, 'r');
$startid = fread($fh, filesize($f));
fclose($fh);

// Get laststart
$f = 'laststart.txt';
$fh = fopen($f, 'r');
$laststart = fread($fh, filesize($f));
fclose($fh);

// Get found logs
$f = 'found.log';
$fh = fopen($f, 'r');
$found = fread($fh, filesize($f));
fclose($fh);

// Get error logs
$f = 'errors.log';
$fh = fopen($f, 'r');
$errors = fread($fh, filesize($f));
fclose($fh);

if (php_sapi_name() === 'cli') {
  echo 'Comments: ' . $comments . PHP_EOL;
  echo 'StartID: ' . $startid . PHP_EOL;
  echo 'LastStart: ' . $laststart . PHP_EOL;
  echo 'Found:' . PHP_EOL;
  echo $found . PHP_EOL;
  echo 'Errors:' . PHP_EOL;
  echo $errors . PHP_EOL;
} else { ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Minion</title>
</head>
<body>
<h1>Status</h1>
<pre><code><?= $comments ?></code></pre>
<hr>
<b>StartID:</b> <?= $startid ?><br>
<b>LastStart:</b> <?= $laststart ?><br>
<b>Found:</b><br>
<pre><code><?= $found ?></code></pre>
<b>Errors:</b><br>
<pre><code><?= $errors ?></code></pre>
</body>
</html>
<?php }

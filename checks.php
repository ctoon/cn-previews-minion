<?php
// This files checks if connexion to CN works and if we have
// all our base files. It will also create all we need at first run.

// Check if we can reach CN's backend
$n = rand(0, 1000000);
$url = 'http://www.cartoonnetwork.com/cnservice/cartoonsvc/content/xml/getContentById.do?contentId=' . $n;
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$output = curl_exec($ch);
curl_close($ch);
$x = new SimpleXMLElement($output);
// contentId in XML should be the same one as we requested
if ($x->attributes()['contentId'] != $n) {
  echo 'ERROR: Test with CN backend failed!' . PHP_EOL;
  exit(1);
}

// We need to know where to start
if (!file_exists('startid.txt')) {
  echo 'ERROR: Create a startid.txt!' . PHP_EOL;
  exit(1);
}

// We need our config
if (!file_exists('config.ini')) {
  echo 'ERROR: Create a config.ini!' . PHP_EOL;
  exit(1);
}

// Create empty files that we'll append to
$f = 'found.log';
if (!file_exists($f)) {
  $fh = fopen($f, 'w');
  fwrite($fh, '');
  fclose($fh);
}
$f = 'errors.log';
if (!file_exists($f)) {
  $fh = fopen($f, 'w');
  fwrite($fh, '');
  fclose($fh);
}

// This one is just for the statuses
$f = 'comments.txt';
if (!file_exists($f)) {
  $fh = fopen($f, 'w');
  fwrite($fh, gethostname());
  fclose($fh);
}
